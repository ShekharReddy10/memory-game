const gameContainer = document.getElementById("game");

const COLORS = [
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
  "./gifs/7.gif",
  "./gifs/8.gif",
  "./gifs/9.gif",
  "./gifs/10.gif",
  "./gifs/11.gif",
  "./gifs/12.gif",
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
  "./gifs/7.gif",
  "./gifs/8.gif",
  "./gifs/9.gif",
  "./gifs/10.gif",
  "./gifs/11.gif",
  "./gifs/12.gif"
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);
    // call a function handleCardClick when a div is previous on
    newDiv.style.backgroundImage = `url("./gifs/default.gif")`;
    newDiv.style.backgroundPositin = "center center";
    newDiv.style.backgroundRepeat = "no-repeat";
    newDiv.style.backgroundSize = "cover";
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

let previous = null;
let clickCount = 0;
// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was previou
  if (clickCount < 2 && event.target.style.backgroundImage == `url("./gifs/default.gif")`) {
    let gif = `"${event.target.classList[0]}"`;
    current = event.target;
    console.log(gif);
    console.log(event.target);
    current.style.transform = "scaleX(-1)";
    current.style.transition = "0.6s";
    current.style.backgroundImage = `url(${gif})`;
    clickCount += 1;
    if (clickCount === 1) {
      previous = current;
    }
    else {
      setTimeout(() => {
        if (previous.classList[0] !== current.classList[0]) {
          previous.style.transform = "scaleX(1)";
          previous.style.transition = "0.6s";
          previous.style.backgroundImage = `url("./gifs/default.gif")`;
          current.style.transform = "scaleX(1)";
          current.style.transition = "0.6s";
          current.style.backgroundImage = `url("./gifs/default.gif")`;
        }
        clickCount = 0;
      }, 1 * 1000)
    }
  }
}

// when the DOM loads
createDivsForColors(shuffledColors);
